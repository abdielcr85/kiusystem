import Vue from 'vue'
import Vuex from 'vuex'
const imageSource = "https://jsonplaceholder.typicode.com/photos?albumId=1";
const axios = require('axios');
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    imageList:[],
    selectedImage:''
  },
  getters:{
    getImages: (state) => {
      return state.imageList;
    },
    getSelectedImage: (state) =>{
      return state.selectedImage;
    },
    getImageDetail: (state) =>{
      if(state.selectedImage)
        for(let image of state.imageList)
          if(image.id==state.selectedImage)
            return image
      return null;
    },
  },
  mutations: {
    updateImageList:function(state,newImageList){
      state.imageList=newImageList;
    },
    updateSelectedImage:function(state,selectedImage){
      state.selectedImage=selectedImage;
    }
  },
  actions: {
    loadImages({commit}){
      axios.get(imageSource)
        .then(function (response) {
          
          commit('updateImageList',response.data)
        })
        .catch(function (error) {
          alert('Error in request',error);
        })
    },
    selectImage({commit},imageId){
      commit('updateSelectedImage',imageId)
    }
  }
})
